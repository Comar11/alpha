from odoo import api, fields, models


class ACFaisabilite(models.Model):
    _name="ac.faisabilite"
    _description="Etude de faisabilite"

    echantillon=fields.Many2one("ac.impact",string="Echantillon", required=True)
    charge=fields.Float("Charge million$",required=True)
    date=fields.Date('Date'):
    
    eie=fields.Float(string="EIE million$",required=True)
    gain=fields.Float("Gain estimé million$",required=True,default=1)
    state=fields.Selection([('O','opportunité'),('P','Perte')],string="State",readonly=True, tracking=True)
    # rendement=fields.Char("Rendement",compute="check_rendement")
    resultat=fields.Char("benefice million$",required=True,compute="check_rendement")

    @api.depends('eie','gain','charge','resultat','echantillon')
 
    def check_rendement (self):
        for rec in self:
            res=0.0000
            rec.eie=rec.echantillon.impact*0.1
            if rec.gain == rec.eie :
                rec.state='P'
                rec.resultat='Sans benefice'
            else: 
                res=rec.charge/((rec.gain-((rec.eie)))/rec.gain)
                rec.resultat=str(res)
                if rec.charge > rec.gain :
                    rec.state="P"
                elif res>rec.eie*2 :
                    rec.state="O"
    

                
        
       

